Building LibreWolf with git:

Clone the git repository via https:
git clone --recursive https://gitlab.com/librewolf-community/browser/source.git

or ssh:

git clone --recursive git@gitlab.com:librewolf-community/browser/source.git

cd into it, build the LibreWolf tarball, bootstrap the build environment, and finally, perform the build:

cd source
make all
make bootstrap
make build

After that, you can either build a tarball from it, or run it:

make package
make run
